import torch
from transformers import DistilBertTokenizer, DistilBertForSequenceClassification, pipeline


tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-uncased-finetuned-sst-2-english")
model = DistilBertForSequenceClassification.from_pretrained("distilbert-base-uncased-finetuned-sst-2-english")

torch.set_num_threads(1)


class EmotionClassifier:

    model = pipeline(model="seara/rubert-tiny2-ru-go-emotions")

    @classmethod
    def analyze_sentiment(cls, text: str):
        """
        Предсказание эмоций по тексту.
        Args:
            text (str): Текст для анализа эмоций.
        """
        result = cls.model(text)
        return result


# inputs = tokenizer(text, return_tensors="pt")
#
# with torch.no_grad():
#     logits = model(**inputs).logits
#
# predicted_class_id = logits.argmax().item()
# sentiment_label = model.config.id2label[predicted_class_id]
#
# if sentiment_label == 'POSITIVE':
#     return 1
# else:
#     return 0
