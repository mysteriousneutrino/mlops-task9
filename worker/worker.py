# celery = Celery(__name__)
# celery.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "redis://localhost:6379")
# celery.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", "redis://localhost:6379")

# worker.py
from celery import Celery
import os

app = Celery(
    'tasks',
    # broker='pyamqp://guest@localhost//',
    broker='redis://localhost:6379/0',
    backend='redis://localhost:6379/0'
)

app.conf.task_routes = {
    'app.tasks.analyze_sentiment': {'queue': 'sentiment_queue'},
}

if __name__ == "__main__":
    app.start()
