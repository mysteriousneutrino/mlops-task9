import gradio as gr
import requests

# функция для взаимодействия с API
def analyze_text_via_api(text):
    try:
        response = requests.post("http://localhost:8000/analyze_without/", json={"text": text})
        response.raise_for_status()
        result = response.json()
        return result
    except requests.exceptions.RequestException as e:
        return {"error": f"Failed to analyze sentiment: {str(e)}"}



def sentiment_analysis(text):
    result = analyze_text_via_api(text)
    return result

iface = gr.Interface(
    fn=sentiment_analysis,
    inputs=gr.components.Textbox(lines=2, placeholder="Enter text here..."),
    outputs="Ввод",
    title="Sentiment Analysis",
    description="приложения для анализа тональности текста!"
)

if __name__ == "__main__":
    iface.launch(server_name="0.0.0.0", server_port=7860)
