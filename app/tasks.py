from celery import Celery
from model import EmotionClassifier
from worker.worker import app

celery_app = Celery('tasks',
                    backend='redis://localhost:6379/0',
                    broker='pyamqp://guest@localhost//',
                    )


@celery_app.task(name='analyze_sentiment')
def analyze_sentiment(text):
    print("start_task")
    sentiment = EmotionClassifier.analyze_sentiment(text)
    print("end_task")
    return sentiment
