from fastapi import FastAPI, HTTPException, BackgroundTasks
from pydantic import BaseModel
from celery import Celery
import os

from model import EmotionClassifier

app = FastAPI()


celery_app = Celery('tasks',
    backend='redis://localhost:6379/0',
    broker='redis://localhost:6379/0',
)

class TextData(BaseModel):
    text: str

@app.post("/analyze/")
async def analyze_text(data: TextData):
    task = celery_app.send_task('app.tasks.analyze_sentiment', args=[data.text])
    return {"task_id": task.id}

@app.get("/result/{task_id}")
async def get_result(task_id: str):
    task = celery_app.AsyncResult(task_id)
    if task.state == 'PENDING':
        return {"state": task.state}
    elif task.state != 'FAILURE':
        return {"state": task.state, "result": task.result}
    else:
        return {"state": task.state, "result": str(task.info)}


@app.post("/analyze_without/")
async def analyze_text(data: TextData):
    print("----------------------------------------------------------", data.text)
    return EmotionClassifier.analyze_sentiment(data.text)


@app.post("/predict/")
async def predict_emotion(text_request: TextData, background_tasks: BackgroundTasks):
    async_result = celery_app.send_task("analyze_sentiment", args=[text_request.text])
    result = async_result.get()
    return result